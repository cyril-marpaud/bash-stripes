use anyhow::Result;
use esp_idf_hal::{peripherals::Peripherals, reset};
use log::{error, info};
use oxide_spark::prelude::*;

fn main() -> Result<()> {
	esp_idf_sys::link_patches();

	// Initialize OxidESPark (logging, configuration & threads)
	Init::init(include_str!("full_configuration.toml"))?;

	// Display a friendly message
	info!("Hello, world!");

	// Initialize peripherals
	let peripherals = Peripherals::take().expect("Failed to take peripherals");

	// Instantiate embedded RGB LED
	let rgb_led = LedStripBuilder::new()
		.channel(0)
		.gpio(2)
		.length(1)
		// Set color to red until Wifi connection is up
		.mode(LedStripMode::Full(RED))
		.build()?
		.init()?;

	// Connect to Wifi
	Wifi::init(peripherals.modem)?;

	// Set color to orange until MQTT connection is up
	rgb_led.send(LedStripMode::Full(ORANGE_RED))?;

	// Connect to MQTT broker
	let (data_sender, cmd_receiver) = Mqtt::init()?;

	// Connection is up, set RGB LED color to green
	rgb_led.send(LedStripMode::Full(CHARTREUSE))?;

	// Initialize LED strip
	let led_strip = LedStripBuilder::new()
		.channel(1)
		.gpio(6)
		.length(100)
		.build()?
		.init()?;

	// Initialize I2C bus
	let i2c = I2c::init(
		peripherals.i2c0,
		peripherals.pins.gpio8,
		peripherals.pins.gpio10,
	)?;

	// Instantiate sensors and start measuring
	Sensors::init(i2c, &data_sender, Some(&rgb_led))?;

	// Forever loop
	loop {
		match cmd_receiver.recv()? {
			(topic, Command::LedStrip(m)) => {
				info!("Received LedStrip command: {m:?} on topic {topic}");
				led_strip.send(m)?
			}
			(_, Command::System(SystemCommand::Reset)) => {
				info!("Received System command: Reset");
				reset::restart();
			}
			c => error!("Received unhandled command: {c:?}"),
		}
	}
}
