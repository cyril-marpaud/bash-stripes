use config::{File, FileFormat};
use once_cell::sync::Lazy;
use oxide_spark_utils::config::{Config, CONFIG_FILE};

use crate::utils::error::OxideSparkError;

/// The global configuration object. It is automatically initialized on startup.
pub static CONFIG: Lazy<Config> = Lazy::new(|| {
	config::Config::builder()
		.add_source(File::from_str(
			CONFIG_FILE
				.get()
				.ok_or(OxideSparkError::ConfigUninitialized)
				.unwrap(),
			FileFormat::Toml,
		))
		.build()
		.map_err(OxideSparkError::ConfigBuild)
		.unwrap()
		.try_deserialize()
		.map_err(OxideSparkError::ConfigDeserialization)
		.unwrap()
});
