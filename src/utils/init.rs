use esp_idf_hal::task::thread::ThreadSpawnConfiguration;
use log::info;
use oxide_spark_utils::config::Config;

use crate::utils::{
	config::CONFIG,
	error::{OxideSparkError, OxideSparkResult},
	log::Log,
};

pub struct Init;

impl Init {
	/// Sets OxidESPark's configuration and initialize logging facilities.
	///
	/// ```
	/// let config = include_str!("configuration.toml");
	///
	/// Init::init(config).unwrap();
	/// ```
	///
	/// See [`Config`](oxide_spark_utils::config::Config) for more information.
	pub fn init(configuration: &'static str) -> OxideSparkResult<()> {
		Config::set(configuration);

		Log::init()?;
		info!("Configuration:\n{:#?}", *CONFIG);

		ThreadSpawnConfiguration {
			name: None,
			stack_size: 7168,
			priority: 1,
			inherit: false,
			pin_to_core: None,
		}
		.set()
		.map_err(OxideSparkError::ThreadConfiguration)
	}
}
