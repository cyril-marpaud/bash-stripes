use esp_idf_svc::log::EspLogger;

use crate::utils::{
	config::CONFIG,
	error::{OxideSparkError, OxideSparkResult},
};

static LOGGER: EspLogger = EspLogger;

pub(crate) struct Log;

impl Log {
	pub(crate) fn init() -> OxideSparkResult<()> {
		let log_level = CONFIG.esp.log_level.clone().into();
		let log_target = CONFIG.esp.log_level.get_target();

		LOGGER
			.set_target_level(log_target, log_level)
			.map_err(OxideSparkError::LogSetTargetLevel)?;

		log::set_logger(&LOGGER).map(|()| LOGGER.initialize())?;
		log::set_max_level(log_level);

		Ok(())
	}
}
