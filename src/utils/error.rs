use std::{
	io,
	sync::mpsc::{RecvError, SendError},
};

use config::ConfigError;
use esp_idf_sys::EspError;
use log::{error, SetLoggerError};
use oxide_spark_utils::{
	command::{led_strip_mode::LedStripMode, Command},
	measure::MeasureKind,
	sensor_kind::SensorKind,
};
use thiserror::Error;
use ws2812_esp32_rmt_driver::Ws2812Esp32RmtDriverError;

use crate::models::Metric;

pub type OxideSparkResult<T> = core::result::Result<T, OxideSparkError>;

#[derive(Debug, Error)]
pub enum OxideSparkError {
	#[error("Peripheral failure")]
	Peripherals,

	// Configuration
	#[error("Configuration build failure")]
	ConfigBuild(#[source] ConfigError),
	#[error("Configuration deserialization failure")]
	ConfigDeserialization(#[source] ConfigError),
	#[error("Configuration file has not been set")]
	ConfigUninitialized,

	// Log
	#[error("Log initialization failure")]
	LogInitialization(#[from] SetLoggerError),
	#[error("Log set target level failure")]
	LogSetTargetLevel(#[source] EspError),
	#[error("Log set target level failure")]
	LogCurrentExe(#[source] io::Error),
	#[error("Log filename failure")]
	LogFilename,
	#[error("Log filename to_str failure")]
	LogFilenameToStr,

	// LED strip
	#[error("Failed to create ws2812 device")]
	LedStripDeviceCreation(#[source] Ws2812Esp32RmtDriverError),
	#[error("Failed to create ws2812 device")]
	LedStripWrite(#[source] Ws2812Esp32RmtDriverError),
	#[error("LED strip channel disconnected")]
	LedStripChannelDisconnected,
	#[error("LED strip already initialized (no command sender)")]
	LedStripNoCmdSender,
	#[error("LED strip command is unknown: {0:?}")]
	LedStripUnhandledCommand(LedStripMode),
	#[error("LED strip channel, gpio & length must be set)")]
	LedStripMissingField,
	#[error("LED strip length cannot be zero")]
	LedStripNullLength,
	#[error("Led Strip indicator is out of range")]
	LedStripIndicatorOutOfRange,
	#[error("Sensor data send failure")]
	LedStripSend(#[source] SendError<LedStripMode>),

	// Sensors
	#[error("No sensors found in TOML configuration")]
	SensorListEmpty,
	#[error("Sensor data send failure")]
	SensorDataSend(#[source] SendError<Metric>),
	#[error("Sensor sampling failure")]
	SensorSampling(#[source] anyhow::Error),
	#[error("Wrong metric ({measure_kind}) for sensor {sensor_kind} (id: {sensor_id})")]
	SensorUnknownMetric {
		measure_kind: MeasureKind,
		sensor_kind: SensorKind,
		sensor_id: String,
	},
	#[error("Sensor strip channel disconnected")]
	SensorChannelDisconnected,
	#[error("Sensor {0} ({1}) is too slow, consider decreasing its sampling frequency")]
	SensorTooSlow(String, SensorKind),
	#[error("TSL sensor power on failure")]
	TslPowerOn,

	// I2C
	#[error("I2C driver creation failure")]
	I2cDriverCreation(#[source] EspError),
	#[error("I2C shared-bus creation")]
	I2cSharedBus,

	// MQTT
	#[error("MQTT client creation failure")]
	MqttClientCreation(#[source] EspError),
	#[error("MQTT command thread send failure")]
	MqttCommandSend(#[source] SendError<Command>),
	#[error("MQTT command thread StripCommand deserialization failure")]
	MqttCommandDeserialization(#[source] serde_json::Error),
	#[error("MQTT configuration not found")]
	MqttConfigurationNotFound,
	#[error("MQTT command thread connection failure: {0}")]
	MqttConnectionFailure(#[source] EspError),
	#[error("MQTT topic subscription failure")]
	MqttTopicSubscription(#[source] EspError),
	#[error("MQTT data thread send failure")]
	MqttSend(#[source] EspError),
	#[error("MQTT channel disconnected")]
	MqttChannelDisconnected(#[from] RecvError),

	// Wifi
	#[error("Wifi configuration not found")]
	WifiConfigurationNotFound,
	#[error("ESP system event loop failure")]
	WifiEventLoop(#[source] EspError),
	#[error("NVS partition failure")]
	WifiNvsPartition(#[source] EspError),
	#[error("Wifi device creation failure")]
	WifiDeviceCreation(#[source] EspError),
	#[error("Wifi driver configuration failure")]
	WifiDriverConfiguration(#[source] EspError),
	#[error("Failed to start wifi driver")]
	WifiDriverStart(#[source] EspError),
	#[error("Wifi driver initialization failure")]
	WifiDriverInitialization(#[source] EspError),
	#[error("Wifi driver not connected")]
	WifiDriverNotConnected,
	#[error("Wifi access point & station modes both disabled")]
	WifiApAndStaDisabled,
	#[error("Wifi connection error")]
	WifiConnection(#[source] EspError),
	#[error("Wifi driver connection query failure")]
	WifiDriverConnectionQuery(#[source] EspError),

	#[error("Misc error: {0}")]
	Misc(#[from] anyhow::Error),
	#[error("Thread configuration failure: {0}")]
	ThreadConfiguration(#[source] EspError),
}
