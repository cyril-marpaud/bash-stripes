//! A collection of structures to interact with the physical world.

pub mod led_strip;
pub(crate) mod sensor;

pub use sensor::metric::Metric;
