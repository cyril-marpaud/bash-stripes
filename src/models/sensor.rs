pub mod metric;

use std::{collections::HashMap, sync::mpsc::SyncSender};

use log::error;
use oxide_spark_utils::{config::SensorConfig, measure::MeasureKind, sensor_kind::SensorKind};

use crate::{
	models::sensor::metric::Metric,
	network::i2c::I2CProxy,
	sensors::{shtc3::Shtc3, tsl2561::Tsl2561},
	traits::{measurable::Measurable, unwrap::UnwrapPrint},
	utils::error::*,
};

pub(crate) struct Sensor {
	pub id: String,
	pub kind: SensorKind,
	metrics: HashMap<MeasureKind, String>,
	sender: SyncSender<Metric>,
	sensor: Box<dyn Measurable + Send + Sync>,
}

impl Sensor {
	pub fn measure(&mut self) -> OxideSparkResult<()> {
		let measures = self.sensor.sample()?;

		let metrics = measures
			.into_iter()
			.filter_map(|measure| match Metric::try_from_measure(measure, self) {
				Ok(m) => Some(m),
				Err(e) => {
					error!("{e}");
					None
				}
			})
			.collect::<Vec<Metric>>();

		metrics.into_iter().for_each(|m| {
			self.sender
				.send(m)
				.unwrap_or_print(OxideSparkError::SensorDataSend)
		});

		Ok(())
	}
}

impl From<(&SensorConfig, I2CProxy, SyncSender<Metric>)> for Sensor {
	fn from((sensor_cfg, i2c_bus, sender): (&SensorConfig, I2CProxy, SyncSender<Metric>)) -> Self {
		let sensor: Box<dyn Measurable + Send + Sync> = match sensor_cfg.kind {
			SensorKind::Bme280 => todo!(),
			SensorKind::Mpu9250 => todo!(),
			SensorKind::Shtc3 => Box::new(Shtc3::new(i2c_bus)),
			SensorKind::Tsl2561 => Box::new(Tsl2561::new(i2c_bus)),
		};

		Sensor {
			kind: sensor_cfg.kind,
			metrics: sensor_cfg.metrics.clone(),
			sensor,
			sender,
			id: sensor_cfg.id.clone(),
		}
	}
}
