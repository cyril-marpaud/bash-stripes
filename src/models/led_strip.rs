//! WS2812 (RGB LED strips) related structures

use std::{
	self,
	sync::mpsc::{sync_channel, Receiver, SyncSender, TryRecvError},
	thread,
};

use esp_idf_hal::reset;
use log::{error, info};
use oxide_spark_utils::command::led_strip_mode::{Direction, LedStripMode, SnakeMode};
use smart_leds::{
	colors::{BLACK, WHITE},
	hsv::{hsv2rgb, Hsv},
	SmartLedsWrite, RGB,
};
use ws2812_esp32_rmt_driver::Ws2812Esp32Rmt;

use crate::{
	traits::unwrap::UnwrapPrint,
	utils::error::{OxideSparkError, OxideSparkResult},
};

/// Builder for [`LedStrip`](crate::models::led_strip::LedStrip).
///
/// It can be used to control the board's embedded RGB LED:
///
/// ```
/// let rgb_led = LedStripBuilder::new()
/// 	.channel(0)
/// 	.gpio(2)
/// 	.length(1)
/// 	.mode(LedStripMode::Full(Color::Red))
/// 	.build()
/// 	.unwrap()
/// 	.init()
/// 	.unwrap();
/// ```
#[derive(Default)]
pub struct LedStripBuilder {
	channel: Option<u8>,
	gpio: Option<u32>,
	indicator: Option<usize>,
	length: Option<usize>,
	mode: Option<LedStripMode>,
}

impl LedStripBuilder {
	/// Create a new builder
	pub fn new() -> Self {
		Default::default()
	}

	/// Set the LED strip channel. There are 4 channels available (0-3).
	pub fn channel(mut self, channel: u8) -> Self {
		self.channel = Some(channel);
		self
	}

	/// Set the LED strip light indicator. The corresponding LED will be set to white if any LED is on.
	pub fn indicator(mut self, indicator: usize) -> Self {
		self.indicator = Some(indicator);
		self
	}

	/// Set the LED strip GPIO pin. The embedded RGB LED is connected to GPIO 2.
	pub fn gpio(mut self, gpio: u32) -> Self {
		self.gpio = Some(gpio);
		self
	}

	/// Set the LED strip length.
	pub fn length(mut self, length: usize) -> Self {
		self.length = Some(length);
		self
	}

	/// Set the LED strip mode. See [`LedStripMode`](oxide_spark_utils::command::led_strip_mode::LedStripMode) for more information.
	pub fn mode(mut self, mode: LedStripMode) -> Self {
		self.mode = Some(mode);
		self
	}

	/// Build the [`LedStrip`](crate::models::led_strip::LedStrip).
	pub fn build(mut self) -> OxideSparkResult<LedStrip> {
		let (Some(channel_nb), Some(gpio), Some(length @ 1..)) =
			(self.channel.take(), self.gpio.take(), self.length.take())
		else {
			return Err(OxideSparkError::LedStripMissingField);
		};

		match (length, self.indicator) {
			(0, _) => return Err(OxideSparkError::LedStripNullLength),
			(l, Some(i)) if i >= l => return Err(OxideSparkError::LedStripIndicatorOutOfRange),
			_ => (),
		}

		let (cmd_sender, cmd_receiver) = sync_channel(1);

		Ok(LedStrip {
			channel_nb,
			cmd_receiver,
			cmd_sender: Some(cmd_sender),
			indicator: self.indicator.take(),
			gpio,
			length,
			mode: self.mode.take().unwrap_or_default(),
		})
	}
}

/// A WS2812 LED strip. See the [`ws2812_esp32_rmt_driver`](https://crates.io/crates/ws2812-esp32-rmt-driver) crate for more information.
#[derive(Debug)]
pub struct LedStrip {
	channel_nb: u8,
	cmd_receiver: Receiver<LedStripMode>,
	cmd_sender: Option<SyncSender<LedStripMode>>,
	indicator: Option<usize>,
	gpio: u32,
	mode: LedStripMode,
	length: usize,
}

impl LedStrip {
	/// Initialize the LED strip. This returns a [`SyncSender`](std::sync::mpsc::SyncSender) that can be used to send commands to the LED strip.
	pub fn init(mut self) -> OxideSparkResult<SyncSender<LedStripMode>> {
		let sender = self
			.cmd_sender
			.take()
			.ok_or(OxideSparkError::LedStripNoCmdSender)?;

		thread::spawn(move || -> OxideSparkResult<()> {
			info!("LED strip thread started:\n{self:#?}");

			let mut strip = Ws2812Esp32Rmt::new(self.channel_nb, self.gpio)
				.map_err(OxideSparkError::LedStripDeviceCreation)?;

			let mut offset = usize::default();
			let mut pattern = vec![BLACK; self.length];

			let mut rainbow_pattern = Vec::from_iter((usize::MIN..self.length).map(|i| {
				hsv2rgb(Hsv {
					hue: ((((i + offset) % self.length) * u8::MAX as usize) as f32
						/ self.length as f32)
						.round() as u8,
					sat: u8::MAX,
					val: u8::MAX,
				})
			}));

			loop {
				match self.cmd_receiver.try_recv() {
					Ok(m) => {
						info!("Received command: {:?}", m);
						self.mode = m;
					}

					Err(TryRecvError::Empty) => {
						match self.mode {
							LedStripMode::Full(c) => pattern.fill(c),
							LedStripMode::Rainbow(d) => {
								match d {
									Direction::Right => rainbow_pattern.rotate_right(1),
									Direction::Left => rainbow_pattern.rotate_left(1),
								}
								pattern = rainbow_pattern.clone();
							}
							LedStripMode::Snake(m, d) => {
								pattern.fill(BLACK);
								pattern[offset] = match m {
									SnakeMode::Classic(c) => c,
									SnakeMode::Rainbow => rainbow_pattern[offset],
								};

								offset = (offset
									+ match d {
										Direction::Right => 1,
										Direction::Left => self.length - 1,
									}) % self.length;
							}
							c => error!("{}", OxideSparkError::LedStripUnhandledCommand(c)),
						};

						if let (Some(i), true) = (
							self.indicator,
							pattern
								.iter()
								.any(|RGB { r, g, b }| r > &0 || g > &0 || b > &0),
						) {
							pattern[i] = WHITE;
						}

						strip
							.write(pattern.clone().into_iter())
							.unwrap_or_print(OxideSparkError::LedStripWrite);
					}

					Err(TryRecvError::Disconnected) => {
						error!("{}", OxideSparkError::LedStripChannelDisconnected);
						reset::restart();
					}
				}
			}
		});

		Ok(sender)
	}
}
