use oxide_spark_utils::measure::*;

use crate::{
	models::sensor::Sensor,
	utils::{config::CONFIG, error::*},
};

/// The aggregation of a [`Measure`](oxide_spark_utils::measure::Measure), its ID and the ID of the sensor it belongs to.
pub struct Metric {
	/// The ID of the sensor which took the measure.
	pub sensor_id: String,
	/// The ID of the metric.
	pub metric_id: String,
	/// The measure itself.
	pub measure: Measure,
}

impl Metric {
	/// Returns the MQTT topic a Metric will be published to (currently `esp_id/sensor_id/metric_id`).
	pub fn topic(&self) -> String {
		format!("{}/{}/{}", CONFIG.esp.id, self.sensor_id, self.metric_id)
	}

	pub(crate) fn try_from_measure(measure: Measure, sensor: &Sensor) -> OxideSparkResult<Self> {
		let measure_kind = MeasureKind::from(measure);

		sensor
			.metrics
			.get(&measure_kind)
			.map(|metric_id| Metric {
				sensor_id: sensor.id.clone(),
				metric_id: metric_id.clone(),
				measure,
			})
			.ok_or(OxideSparkError::SensorUnknownMetric {
				measure_kind,
				sensor_kind: sensor.kind,
				sensor_id: sensor.id.clone(),
			})
	}
}

impl ToString for Metric {
	fn to_string(&self) -> String {
		self.measure.to_string()
	}
}
