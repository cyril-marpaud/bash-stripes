use std::sync::Mutex;

use esp_idf_hal::{
	gpio::{Gpio10, Gpio8},
	i2c::{I2cConfig, I2cDriver, I2C0},
	prelude::FromValueType,
};
use shared_bus::{BusManager, I2cProxy};

use crate::utils::error::{OxideSparkError, OxideSparkResult};

pub(crate) type SharedI2CBus = BusManager<Mutex<I2cDriver<'static>>>;
pub(crate) type I2CProxy = I2cProxy<'static, Mutex<I2cDriver<'static>>>;

/// A thread-safe I2C bus. See the [`shared_bus`] crate for more information.
pub struct I2c {
	pub bus: &'static SharedI2CBus,
}

impl I2c {
	/// Initializes the board's I2C bus. SCL pin is GPIO8 and SDA pin is GPIO10.
	///
	/// ```
	/// let peripherals = Peripherals::take().unwrap();
	///
	/// let i2c = I2c::init(
	/// 	peripherals.i2c0,
	/// 	peripherals.pins.gpio8,
	/// 	peripherals.pins.gpio10,
	/// )
	/// .unwrap();
	/// ```
	pub fn init(i2c: I2C0, scl: Gpio8, sda: Gpio10) -> OxideSparkResult<Self> {
		let config = I2cConfig::new()
			.baudrate(400.kHz().into())
			.sda_enable_pullup(true)
			.scl_enable_pullup(true);

		let i2c_driver =
			I2cDriver::new(i2c, sda, scl, &config).map_err(OxideSparkError::MqttClientCreation)?;

		shared_bus::new_std!(I2cDriver = i2c_driver)
			.map(|bus| Self { bus })
			.ok_or(OxideSparkError::I2cSharedBus)
	}
}
