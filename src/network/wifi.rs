use std::{thread, time::Duration};

use anyhow::anyhow;
use embedded_svc::wifi::{ClientConfiguration, Configuration};
use esp_idf_hal::{peripheral::Peripheral, reset};
use esp_idf_svc::{
	eventloop::EspSystemEventLoop, nvs::EspDefaultNvsPartition, timer::EspTimerService,
	wifi::EspWifi,
};
use esp_idf_sys as _;
use log::{error, info};

use crate::utils::{
	config::CONFIG,
	error::{OxideSparkError, OxideSparkResult},
};

/// A structure representing the WiFi client.
pub struct Wifi;

impl Wifi {
	/// Initialize the WiFi client.
	///
	/// ```
	/// let peripherals = Peripherals::take().unwrap();
	///
	/// Wifi::init(peripherals.modem).unwrap();
	/// ```
	///
	/// See [`WifiConfig`](oxide_spark_utils::config::WifiConfig) for more information.
	pub fn init(
		modem: impl Peripheral<P = esp_idf_hal::modem::Modem> + 'static,
	) -> OxideSparkResult<()> {
		let wifi_config = CONFIG
			.wifi
			.as_ref()
			.ok_or(OxideSparkError::WifiConfigurationNotFound)?;

		let sysloop = EspSystemEventLoop::take().map_err(OxideSparkError::WifiEventLoop)?;
		let nvs = EspDefaultNvsPartition::take().map_err(OxideSparkError::WifiNvsPartition)?;
		let mut wifi_driver = EspWifi::new(modem, sysloop.clone(), Some(nvs))
			.map_err(OxideSparkError::WifiDeviceCreation)?;

		wifi_driver
			.set_configuration(&Configuration::Client(ClientConfiguration {
				ssid: wifi_config.ssid.as_str().into(),
				password: wifi_config.pwd.as_str().into(),
				auth_method: wifi_config.auth_method,
				..Default::default()
			}))
			.map_err(OxideSparkError::WifiDriverConfiguration)?;

		info!("Starting wifi driver");

		wifi_driver
			.start()
			.map_err(OxideSparkError::WifiDriverStart)?;

		loop {
			match wifi_driver.is_started() {
				Ok(true) => break,
				Ok(false) => {
					info!("{}", OxideSparkError::WifiApAndStaDisabled);
					thread::sleep(Duration::from_secs(1));
				}
				Err(e) => {
					error!("{}", OxideSparkError::WifiDriverInitialization(e));
					reset::restart();
				}
			}
		}

		info!("Wifi driver started");

		wifi_driver
			.connect()
			.map_err(OxideSparkError::WifiConnection)?;

		info!("Waiting for wifi connection");

		let timer = EspTimerService::new().map_err(|e| OxideSparkError::Misc(anyhow!(e)))?;
		let start = timer.now();

		loop {
			match wifi_driver.is_up() {
				Ok(true) => break,
				Ok(false) => {
					info!("{}", OxideSparkError::WifiDriverNotConnected);
					thread::sleep(Duration::from_secs(1));
				}
				Err(e) => error!("{}", OxideSparkError::WifiDriverConnectionQuery(e)),
			}

			// If wifi does not come up after a few seconds, reboot...
			if timer.now() > start + Duration::from_secs(wifi_config.timeout) {
				info!("Wifi took too long to connect, rebooting...");
				reset::restart();
			}
		}

		info!("Wifi driver connected");

		// Free the variable but keep the underlying resource alive
		std::mem::forget(wifi_driver);

		Ok(())
	}
}
