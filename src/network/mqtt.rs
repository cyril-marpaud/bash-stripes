use std::{
	self,
	sync::mpsc::{channel, sync_channel, Receiver, SendError, SyncSender},
	thread,
	time::Duration,
};

use embedded_svc::mqtt::client::{Connection, Event::*, Message, QoS};
use esp_idf_svc::mqtt::client::{EspMqttClient, MqttClientConfiguration};
use log::{error, info};
use oxide_spark_utils::{command::Command, config::MqttConfig};

use crate::{
	models::sensor::metric::Metric,
	traits::unwrap::UnwrapPrint,
	utils::{
		config::CONFIG,
		error::{OxideSparkError, OxideSparkResult},
	},
};

type MqttCommandReceiver = Receiver<(String, Command)>;
type MqttMetricSender = SyncSender<Metric>;

/// A structure representing the MQTT client.
pub struct Mqtt;

impl Mqtt {
	fn get_url(mqtt_conf: &MqttConfig) -> String {
		format!("mqtt://{}:{}", mqtt_conf.ip, mqtt_conf.port)
	}

	/// Initialize the MQTT client.
	///
	/// ```
	/// let (data_sender, cmd_receiver) = Mqtt::init().unwrap();
	/// ```
	///
	/// `data_sender` is the sending half of a `Metric` channel while `cmd_receiver` is the receiving half of a `Command`s channel.
	/// `Command`s are packed with the topic they were received on.
	///
	/// See [`MqttConfig`](oxide_spark_utils::config::MqttConfig) for more information.
	pub fn init() -> OxideSparkResult<(MqttMetricSender, MqttCommandReceiver)> {
		let mqtt_conf = CONFIG
			.mqtt
			.as_ref()
			.ok_or(OxideSparkError::MqttConfigurationNotFound)?;

		let (cmd_sender, cmd_receiver) = channel();
		let (data_sender, data_receiver) = sync_channel::<Metric>(mqtt_conf.channel_size);

		let conf = MqttClientConfiguration {
			client_id: Some(&CONFIG.esp.id),
			keep_alive_interval: Some(Duration::from_secs(120)),
			..Default::default()
		};

		let (mut client, mut connection) =
			EspMqttClient::new_with_conn(Self::get_url(mqtt_conf), &conf)
				.map_err(OxideSparkError::MqttClientCreation)?;

		thread::spawn(move || {
			info!("MQTT command thread started");

			loop {
				if let Some(res) = connection.next() {
					res.map(|evt| match evt {
						Received(m) => {
							serde_json::from_slice::<Command>(m.data())
								.map(|cmd| {
									cmd_sender
										.send((m.topic().unwrap_or_default().to_owned(), cmd))
										.unwrap_or_print(|_| {
											OxideSparkError::MqttCommandSend(SendError(cmd))
										})
								})
								.unwrap_or_print(OxideSparkError::MqttCommandDeserialization);
						}
						evt => info!("MQTT event received: {evt:?}"),
					})
					.unwrap_or_print(OxideSparkError::MqttConnectionFailure);
				}
			}
		});

		loop {
			let subscription = client.subscribe(&format!("{}/+", &CONFIG.esp.id), QoS::ExactlyOnce);

			match subscription {
				Ok(id) => {
					info!("MQTT subscribe message ID: {id}");
					break;
				}
				Err(e) => {
					error!("{}", OxideSparkError::MqttTopicSubscription(e));
					thread::sleep(Duration::from_secs(5));
				}
			}
		}

		if !CONFIG.sensors.is_empty() {
			thread::spawn(move || -> OxideSparkResult<()> {
				info!("MQTT data thread started");

				loop {
					let metric = data_receiver.recv()?;

					client
						.publish(
							&metric.topic(),
							QoS::AtMostOnce,
							false,
							metric.to_string().as_bytes(),
						)
						.map(|id| info!("MQTT publish message ID: {id}"))
						.unwrap_or_print(OxideSparkError::MqttSend);
				}
			});
		}

		Ok((data_sender, cmd_receiver))
	}
}
