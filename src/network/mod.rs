//! Connectivity-related structures.

pub mod i2c;
pub mod mqtt;
pub mod wifi;
