use std::error::Error;

use anyhow::anyhow;
use embedded_hal::blocking::i2c::{Read, Write};
use esp_idf_hal::delay::FreeRtos;
use oxide_spark_utils::measure::Measure;
use shtcx::{PowerMode, ShtC3};

use crate::{
	traits::measurable::Measurable,
	utils::error::{OxideSparkError, OxideSparkResult},
};

/// A temperature and humidity sensor.
pub struct Shtc3<I2C> {
	device: ShtC3<I2C>,
}

impl<I2C: Read<Error = E> + Write<Error = E>, E: Error> Shtc3<I2C> {
	pub fn new(i2c: I2C) -> Self {
		Self {
			device: shtcx::shtc3(i2c),
		}
	}
}

impl<I2C: Read<Error = E> + Write<Error = E>, E: Error> Measurable for Shtc3<I2C> {
	fn sample(&mut self) -> OxideSparkResult<Vec<Measure>> {
		self.device
			.measure(PowerMode::NormalMode, &mut FreeRtos)
			.map(|m| {
				vec![
					Measure::Temperature(m.temperature.as_degrees_celsius()),
					Measure::Humidity(m.humidity.as_percent()),
				]
			})
			.map_err(|e| OxideSparkError::SensorSampling(anyhow!(format!("{e:?}"))))
	}
}
