//! An abstration representing the connected I2C sensors

pub(crate) mod shtc3;
pub(crate) mod tsl2561;

use std::{
	sync::mpsc::SyncSender,
	thread::{self},
	time::{Duration, Instant},
};

use log::{error, info};
use oxide_spark_utils::command::led_strip_mode::LedStripMode;
use smart_leds::colors::{BLACK, CHARTREUSE};

use crate::{
	models::{sensor::Sensor, Metric},
	network::i2c::I2c,
	traits::unwrap::UnwrapPrint,
	utils::{
		config::CONFIG,
		error::{OxideSparkError, OxideSparkResult},
	},
};

pub struct Sensors;

impl Sensors {
	/// Initialize all I2C sensors.
	///
	///
	/// ```
	/// let peripherals = Peripherals::take().unwrap();
	///
	/// let (data_sender, data_receiver) = sync_channel::<Metric>(10);
	///
	/// let i2c = I2c::init(
	/// 	peripherals.i2c0,
	/// 	peripherals.pins.gpio8,
	/// 	peripherals.pins.gpio10,
	/// )
	/// .unwrap();
	///
	/// Sensors::init(i2c, &data_sender, None).unwrap();
	/// ```
	///
	/// See [`SensoConfig`](oxide_spark_utils::config::SensorConfig) for more information.
	pub fn init(
		i2c: I2c,
		data_sender: &SyncSender<Metric>,
		rgb_led: Option<&SyncSender<LedStripMode>>,
	) -> OxideSparkResult<()> {
		if CONFIG.sensors.is_empty() {
			return Err(OxideSparkError::SensorListEmpty);
		}

		CONFIG.sensors.iter().for_each(|sensor_config| {
			let rgb_led = rgb_led.cloned();
			let mut sensor =
				Sensor::from((sensor_config, i2c.bus.acquire_i2c(), data_sender.clone()));

			thread::spawn(move || {
				info!("Sensor sampling thread started");

				let mut last_measure = Instant::now();
				let period_ms =
					Duration::from_millis(f64::round(1000.0 / sensor_config.freq as f64) as u64);

				loop {
					sensor.measure().unwrap_or_else(|e| {
						error!("{e}");

						if let Some(rgb_led) = &rgb_led {
							rgb_led
								.send(LedStripMode::Full(BLACK))
								.unwrap_or_print(OxideSparkError::LedStripSend);

							thread::sleep(Duration::from_millis(50));

							rgb_led
								.send(LedStripMode::Full(CHARTREUSE))
								.unwrap_or_print(OxideSparkError::LedStripSend);
						}
					});

					thread::sleep(
						period_ms
							.checked_sub(last_measure.elapsed())
							.unwrap_or_else(|| {
								error!(
									"{}",
									OxideSparkError::SensorTooSlow(sensor.id.clone(), sensor.kind)
								);
								Default::default()
							}),
					);
					last_measure = Instant::now();
				}
			});
		});

		Ok(())
	}
}
