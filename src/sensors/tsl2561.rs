use std::{error::Error, thread, time::Duration};

use embedded_hal::blocking::i2c::{Read, Write, WriteRead};
use log::error;
use oxide_spark_utils::measure::Measure;

use crate::{
	traits::measurable::Measurable,
	utils::error::{OxideSparkError, OxideSparkResult},
};

/// A visible and infrared light sensor.
pub struct Tsl2561<I2C> {
	device: tsl256x::Tsl2561<I2C>,
	i2c: I2C,
}

impl<I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>, E: Error> Tsl2561<I2C> {
	pub fn new(mut i2c: I2C) -> Self {
		let device = tsl256x::Tsl2561::new(&i2c, tsl256x::SlaveAddr::default().addr()).unwrap();

		thread::sleep(Duration::from_millis(10));

		while device.power_on(&mut i2c).is_err() {
			error!("{}", OxideSparkError::TslPowerOn);
			thread::sleep(Duration::from_millis(10));
		}

		thread::sleep(Duration::from_millis(420));

		Self { device, i2c }
	}
}

impl<I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>, E: Error> Measurable
	for Tsl2561<I2C>
{
	fn sample(&mut self) -> OxideSparkResult<Vec<Measure>> {
		let vi = self
			.device
			.visible_and_ir_raw(&mut self.i2c)
			.map(|v| Measure::VisibleInfraredLight(v as f32))
			.map_err(|_| OxideSparkError::SensorSampling(anyhow::anyhow!("vir")))?;

		let i = self
			.device
			.ir_raw(&mut self.i2c)
			.map(|v| Measure::InfraredLight(v as f32))
			.map_err(|_| OxideSparkError::SensorSampling(anyhow::anyhow!("ir")))?;

		Ok(vec![vi, i])
	}
}
