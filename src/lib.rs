//! OxidESPark is a Rust library for the [Rust ESP Board](https://github.com/esp-rs/esp-rust-board)
//! embedding an ESP32-C3 microcontroller (RISC-V). It uses the [ESP-IDF framework](https://docs.espressif.com/projects/esp-idf/en/v5.1.1/esp32c3/get-started/index.html)
//! and provides tools to easily build applications that interact with the physical world.
//!
//! Its main goals are:
//!
//! - Interface various I2C sensors
//! - Allow control of various devices (like LED strips)
//! - Provide connectivity solutions for control/command
//! - Be easily configurable through a simple TOML file
//!
//! Building an application requires using the [ESP-IDF template](https://github.com/esp-rs/esp-idf-template).
//! More information can be found in the repository's [README](https://gitlab.com/cyril-marpaud/oxide-spark/-/blob/main/README.md) file.

#![allow(clippy::tabs_in_doc_comments)]

pub mod models;
pub mod network;
pub mod prelude;
pub mod sensors;
mod traits;
pub mod utils;
