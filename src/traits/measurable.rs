use oxide_spark_utils::measure::Measure;

use crate::utils::error::OxideSparkResult;

/// A trait representing a sensor that can take measures.
pub(crate) trait Measurable {
	/// Returns a [`Vec`] of all the measures the sensor can take.
	fn sample(&mut self) -> OxideSparkResult<Vec<Measure>>;
}
