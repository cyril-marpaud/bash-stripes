use std::result::Result;

use log::error;

use crate::utils::error::OxideSparkError;

pub(crate) trait UnwrapPrint<E> {
	fn unwrap_or_print<C: FnOnce(E) -> OxideSparkError>(self, c: C);
}

impl<E> UnwrapPrint<E> for Result<(), E> {
	fn unwrap_or_print<C: FnOnce(E) -> OxideSparkError>(self, callback: C) {
		self.unwrap_or_else(|e| error!("{}", callback(e)))
	}
}
